<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero lazybg">
		<img src="../assets/bin/images/temp/inner-hero.jpg" alt="Image Alt">
	</div><!-- .hero -->
		
	<div class="body">
	
		<div class="body-header d-bg red-bg">
			<div class="section-header">
				<h1 class="title">Our Irish Pubs</h1>
				<span class="subtitle">A Truly Irish Experience</span>
			</div><!-- .section-header -->
		</div><!-- .body-header -->
		
		<section class="half-section left-half">
			<div class="lazybg" data-src="../assets/bin/images/temp/ov-1.jpg"></div>
			<div class="article-body">
				
				<div class="section-header">
					<span class="title sm h5-style l">About Bridie Molloy's</span>
					<span class="subtitle h3-style">Music. Food. Unforgettable.</span>
				</div><!-- .section-header -->
				
				<p>
					Great food. Great Service. Join us for lunch and return to work feeling ready for the afternoon. From traditional Newfoundland meals, 
					to all your lunch favourites, you’ll love what Bridie Molloy’s has to offer.
				</p>
				
				<a href="#" class="button">Find Out More</a>
				
			</div><!-- .article-body -->
		</section>
		
		<section class="half-section right-half">
			<div class="lazybg" data-src="../assets/bin/images/temp/ov-2.jpg"></div>
			<div class="article-body">
				
				<div class="section-header">
					<span class="title sm h5-style l">History of the Celtic Hearth</span>
					<span class="subtitle h3-style">Steeped In Tradition</span>
				</div><!-- .section-header -->
				
				<p>
					Join us for lunch and return to work feeling ready for the afternoon. 
					From traditional Newfoundland meals, to all your lunch favourites,
					you’ll love what Bridie Molloy’s has to offer.
				</p>
				
				<a href="#" class="button">Find Out More</a>
				
			</div><!-- .article-body -->
		</section>

		<section class="half-section left-half">
			<div class="lazybg" data-src="../assets/bin/images/temp/ov-1.jpg"></div>
			<div class="article-body">
				<div class="section-header">
					<span class="title sm h5-style l">Private Events</span>
					<span class="subtitle h3-style">Book With Us Today.</span>
				</div><!-- .section-header -->
				
				<p>
					Great food. Great Service. Join us for lunch and return to work feeling ready for the afternoon. From traditional Newfoundland meals, 
					to all your lunch favourites, you’ll love what Bridie Molloy’s has to offer.
				</p>
				
				<a href="#" class="button">Find Out More</a>
				
			</div><!-- .article-body -->
		</section>

	</div><!-- .body -->
	
	<?php include('inc/i-promo.php'); ?>

<?php include('inc/i-footer.php'); ?>