<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero lazybg d-bg" data-src="../assets/bin/images/temp/hero.jpg">
		<div class="sw">
			<div class="caption">
				
				<span class="h1-style">One Moment Here is Worth Many More</span>
				
				<p>
					A warm and welcoming Restaurant nestled beneath our traditionally 
					spirited Guinness Pub, ensures that a moment with us is  one to savour.
				</p>
				
				<a href="#" class="button big">Find Out More</a>
				
			</div><!-- .caption -->
		</div><!-- .sw -->
	</div><!-- .hero -->
	
	<div class="body">
		
		<section class="half-section left-half">
			<div class="lazybg" data-src="../assets/bin/images/temp/ov-1.jpg"></div>
			<div class="article-body">
				
				<div class="section-header">
					<span class="title sm h5-style l">Live Music</span>
					<span class="subtitle h3-style">You Will Need to Dance</span>
				</div><!-- .section-header -->
				
				<p>
					Great food. Great Service. Join us for lunch and return to work feeling ready for the afternoon. From traditional Newfoundland meals, 
					to all your lunch favourites, you’ll love what Bridie Molloy’s has to offer.
				</p>
				
				<a href="#" class="button">View Our Calendar</a>
				
			</div><!-- .article-body -->
		</section>
		
		<section class="d-bg lazybg inline-video full" data-src="../assets/bin/images/temp/inline-video.jpg" data-video="//player.vimeo.com/video/105192180?byline=0&portrait=0&autoplay=1">
			<div class="section-header center">
				<span class="h1-style title">An Experience Like No Other</span>
				<i class="t-fa-abs fa-play">Play</i>
			</div><!-- .section-header -->
		</section>

		<section class="half-section right-half">
			<div class="lazybg" data-src="../assets/bin/images/temp/ov-2.jpg"></div>
			<div class="article-body">
				
				<div class="section-header">
					<span class="title sm h5-style l">Weekly Specials</span>
					<span class="subtitle h3-style">Great Food. Great Service.</span>
				</div><!-- .section-header -->
				
				<p>
					Join us for lunch and return to work feeling ready for the afternoon. 
					From traditional Newfoundland meals, to all your lunch favourites,
					you’ll love what Bridie Molloy’s has to offer.
				</p>
				
				<a href="#" class="button">View Our Menu</a>
				
			</div><!-- .article-body -->
		</section>
		
		<section class="half-section left-half">
			<div class="lazybg" data-src="../assets/bin/images/temp/ov-1.jpg"></div>
			<div class="article-body">
				
				<div class="section-header">
					<span class="title sm h5-style l">Private Events</span>
					<span class="subtitle h3-style">Book With Us Today.</span>
				</div><!-- .section-header -->
				
				<p>
					Great food. Great Service. Join us for lunch and return to work feeling ready for the afternoon. From traditional Newfoundland meals, 
					to all your lunch favourites, you’ll love what Bridie Molloy’s has to offer.
				</p>
				
				<a href="#" class="button">Find Out More</a>
				
			</div><!-- .article-body -->
		</section>

	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>