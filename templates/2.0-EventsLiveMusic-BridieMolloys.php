<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero lazybg">
		<img src="../assets/bin/images/temp/inner-hero.jpg" alt="Image Alt">
	</div><!-- .hero -->
		
	<div class="body">
	
		<div class="body-header d-bg red-bg">
			<div class="section-header">
				<h1 class="title">Live Music</h1>
				<span class="subtitle h5-style">You Will Need to Dance</span>
			</div><!-- .section-header -->
		</div><!-- .body-header -->
		
		<section class="d-bg red-bg nopad">
			<div class="sw">
				
				<!-- 
				
					data-src 
						A url that can be passed 
						
							1) an id to retrieve a JSON event string; ?id=3
							
								response:
								
									array(
										'id' => '11',
										'start_time' => 1424566400,
										'end_time' => 1424577200,
										'excerpt' => 'Vivamus posuere elementum leo ac feugiat...',
										'permalink' => 'http://bridiemolloys.com/events/the-pints-11',
										'title' => 'The Pints 11',
										'thumbnail' => 'event-image-1.jpg',
										'next' => '12',
										'prev' => '10',
									);
							
							2) a month/year to retrieve the ids/days of all events in that month; ?month=3&year=2015
							
								response:
								
									array(
										array(
											'id' => 3,
											'day' => 5 //day of month
										),
										
										array(
											'id' => 4,
											'day' => 8 //day of month
										),
										
										array(
											'id' => 5,
											'day' => 12 //day of month
										)
									);
				-->
				
				
				<!--
					data-startmonth and data-startyear determines the starting view of the calendar. If these are not present,
					the calendar will start off in the current month/year according to the user's clock.
					
					It's a good idea (but not required) to set data-startmonth and data-startyear to the month and year of next upcoming event
				-->
				<div class="events-calendar" data-src="./inc/i-events-source.php" data-startmonth="1" data-startyear="2015">
				
					<!-- 
						The contents of events-calendar-tmpl defines the template
						that all events will be rendered with. This specific element
						will be invisible to the user
					-->
					<div class="events-calendar-tmpl">
						
						<div class="event-calendar-event {{has_image}}">
						
							<div class="controls prev-{{prev}} next-{{next}}">
								<span class="prev" data-eventid="{{prev}}">Previous</span>
								<span class="next" data-eventid="{{next}}">Next</span>
							</div><!-- .controls -->
						
							<div class="event-calendar-event-img lazybg" data-src="{{thumbnail}}">
							</div>
							
							<div class="event-calendar-event-content">
							
								<div class="event-title">{{title}}</div>
								<div class="article-head-meta">
									<time class="meta t-fa fa-calendar">{{date_string}}</time>
									<span class="meta t-fa fa-clock-o">{{time_string}}</span>
								</div><!-- .article-head-meta -->
								
								<p>{{excerpt}}</p>
								
								<div class="share-bar">
									Share:
									<a href="http://facebook.com/sharer/sharer.php?u={{permalink}}" class="t-fa-abs fa-facebook">Share {{title}} on Facebook</a>
									<a href="https://twitter.com/home?status={{permalink}}" class="t-fa-abs fa-twitter">Share {{title}} on Twitter</a>
									<a href="https://plus.google.com/share?url={{permalink}}" class="t-fa-abs fa-google-plus">Share {{title}} on Google+</a>
									<a href="https://pinterest.com/pin/create/button/?url={{permalink}}&media={{thumbnail}}" class="t-fa-abs fa-pinterest">Share {{title}} on Pinterest</a>
								</div>
							</div><!-- .event-calendar-event-content -->
							
						</div><!-- .event-calendar-event -->
						
					</div><!-- .events-calendar-tmpl -->
				
					<div class="dp-flat">
					</div><!-- .dp-flat -->
					
					<div class="selected-event">
					</div><!-- .selected-event -->
					
				</div><!-- .events-calendar -->
				
			</div><!-- .sw -->
		</section>
		
		<section class="nopad">
			<div class="pad-40 center">
				<div class="sw">
					<div class="section-header">
						<span class="title sm h5-style">Featured Bands</span>
						<span class="subtitle h3-style">Talented Musical Guests</span>
					</div><!-- .section-header -->
				</div><!-- .sw -->
			</div><!-- .center -->
			
			<div class="upcoming-events">
				<div class="grid eqh fill nopad">
					<div class="col col-3 sm-col-1">
						<a class="item lazybg" href="#" data-src="../assets/bin/images/temp/band-1.jpg">
							
							<div class="caption d-bg">
								<div class="section-header pad-20 xs-pad-5">
									<h3 class="title l">Featured Band One</h3>
									<p>
										Sed lacus dolor, ullamcorper vitae mauris a, vestibulum venenatis mauris. Ut vel elit tempor, facilisis arcu nec, cursus enim. 
										Mauris ante velit, convallis id euismod in, placerat eu velit. In ornare erat vitae sem ultricies egestas. 
									</p>
									<span class="button">Read More</span>
								</div><!-- .section-header -->
							</div><!-- .caption -->
							
						</a>
					</div>
					<div class="col col-3 sm-col-1">
						<a class="item lazybg" href="#" data-src="../assets/bin/images/temp/band-2.jpg">
							
							<div class="caption d-bg">
								<div class="section-header pad-20 xs-pad-5">
									<h3 class="title l">Featured Band Two</h3>
									<p>
										Sed lacus dolor, ullamcorper vitae mauris a, vestibulum venenatis mauris. Ut vel elit tempor, facilisis arcu nec, cursus enim. 
										Mauris ante velit, convallis id euismod in, placerat eu velit. In ornare erat vitae sem ultricies egestas. 
									</p>
									<span class="button">Read More</span>
								</div><!-- .section-header -->
							</div><!-- .caption -->
							
						</a>
					</div>
					<div class="col col-3 sm-col-1">
						<a class="item lazybg" href="#" data-src="../assets/bin/images/temp/band-3.jpg">
							
							<div class="caption d-bg">
								<div class="section-header pad-20 xs-pad-5">
									<h3 class="title l">Featured Band Three</h3>
									<p>
										Sed lacus dolor, ullamcorper vitae mauris a, vestibulum venenatis mauris. Ut vel elit tempor, facilisis arcu nec, cursus enim. 
										Mauris ante velit, convallis id euismod in, placerat eu velit. In ornare erat vitae sem ultricies egestas. 
									</p>
									<span class="button">Read More</span>
								</div><!-- .section-header -->
							</div><!-- .caption -->
							
						</a>
					</div>
				</div>
			</div><!-- .upcoming-events -->
			
		</section><!-- .nopad -->
		
		<section class="nopad">
			<div class="pad-40 center">
				<div class="sw">
					<div class="section-header">
						<span class="title sm h5-style">Upcoming Guests</span>
						<span class="subtitle h3-style">Join Us There</span>
					</div><!-- .section-header -->
				</div><!-- .sw -->
			</div><!-- .center -->
			
			<div class="dark-bg red-bg event-list">
				<a class="event-list-item" data-eventid="1" ref="#">
					<div class="sw">
						<article>
							<div class="img">
								<div class="ar lazybg" data-ar="100">
									<img src="../assets/bin/images/temp/article-1.jpg" alt="article-1">
								</div>
							</div><!-- .img -->
						
							<div class="article-body">
								<div class="article-content">
								
									<div class="article-head-meta">
										<time datetime="2014-12-14" class="meta t-fa fa-calendar">Sunday December 14, 2014</time>
										<span class="meta t-fa fa-clock-o">7:00PM - 10:00PM</span>
										<span class="meta t-fa fa-map-marker">Bridie Molloy's</span>
									</div><!-- .article-head-meta -->
									
									<h2>Praesent eu Laoreet Lectus</h2>
									
									<p>
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
										Integer eget ante sed risus gravida vestibulum id ac ante. Nullam erat dui, viverra quis nulla quis, feugiat viverra diam.
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
										Integer eget ante sed risus gravida vestibulum id ac ante. Nullam erat dui, viverra quis nulla quis, feugiat viverra diam.
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
									</p>	
									
								</div><!-- .article-content -->
								<span class="button grey">Read More</span>
							</div><!-- .article-body -->
							
						</article>
					</div><!-- .sw -->
				</a><!-- .event-list-item -->
				<a class="event-list-item" data-eventid="2" href="#">
					<div class="sw">
						<article>
							<div class="img">
								<div class="ar lazybg" data-ar="100">
									<img src="../assets/bin/images/temp/article-1.jpg" alt="article-1">
								</div>
							</div><!-- .img -->
						
							<div class="article-body">
								<div class="article-content">
									<div class="article-head-meta">
										<time datetime="2014-12-14" class="meta t-fa fa-calendar">Sunday December 14, 2014</time>
										<span class="meta t-fa fa-clock-o">7:00PM - 10:00PM</span>
										<span class="meta t-fa fa-map-marker">Bridie Molloy's</span>
									</div><!-- .article-head-meta -->
									
									<h2>Praesent eu Laoreet Lectus</h2>
									
									<p>
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
										Integer eget ante sed risus gravida vestibulum id ac ante.
									</p>								
								</div><!-- .article-content -->
								<span class="button grey">Read More</span>	
							</div><!-- .article-body -->
							
							
						</article>
					</div><!-- .sw -->
				</a><!-- .event-list-item -->
				<a class="event-list-item" data-eventid="3" href="#">
					<div class="sw">
						<article>
							<div class="img">
								<div class="ar lazybg" data-ar="100">
									<img src="../assets/bin/images/temp/article-1.jpg" alt="article-1">
								</div>
							</div><!-- .img -->
						
							<div class="article-body">
								<div class="article-content">
									<div class="article-head-meta">
										<time datetime="2014-12-14" class="meta t-fa fa-calendar">Sunday December 14, 2014</time>
										<span class="meta t-fa fa-clock-o">7:00PM - 10:00PM</span>
										<span class="meta t-fa fa-map-marker">Bridie Molloy's</span>
									</div><!-- .article-head-meta -->
									
									<h2>Praesent eu Laoreet Lectus eu Laoreet Praesent Lectus</h2>
									
									<p>
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
										Integer eget ante sed risus gravida vestibulum id ac ante. Nullam erat dui, viverra quis nulla quis, feugiat viverra diam.
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
									</p>		
								</div><!-- .article-content -->
								<span class="button grey">Read More</span>
								
							</div><!-- .article-body -->
							
						</article>
					</div><!-- .sw -->
				</a><!-- .event-list-item -->
			</div><!-- .event-list -->
			
			<!--
				data-src 
					A url that will be passed a parameter (id) that contains the last loaded event id
					It should return HTML for several .event-list-item's which will be appened to .event-list
			-->
			
			<div class="load-more-events" data-src="./inc/i-more-events.php">
				<span class="button big">
					<span>Load More</span>
				</span><!-- .button -->
			</div>
			
		</section><!-- .nopad -->
		
	</div><!-- .body -->
	
	<?php include('inc/i-promo.php'); ?>

<?php include('inc/i-footer.php'); ?>