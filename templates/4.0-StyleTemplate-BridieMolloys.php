<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero lazybg d-bg">
		<img src="../assets/bin/images/temp/inner-hero.jpg" alt="Image Alt">
	</div><!-- .hero -->

	<article>
		
		<div class="body">
		
			<div class="body-header d-bg red-bg">
				<div class="section-header">
					<h1 class="title">Styles (h1 Header)</h1>
					<span class="subtitle h5-style">Subtitle</span>
				</div><!-- .section-header -->
			</div><!-- .body-header -->
		
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#">Home</a>
					<a href="#">Styles</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<section>
				<div class="sw">
					<div class="main-body">
						
						<div class="content">
							<div class="article-body">
								<div class="grid">
									<div class="col-2 col sm-col-1">
										<div class="item">
										
											<h1>H1 - 38px Proxima Nova Bold</h1>
											<h2>H2 - 32px Proxima Nova Bold</h2>
											<h3>H3 - 28px Proxima Nova Bold</h3>
											<h4>H4 - 24px Proxima Nova Bold</h4>
											<h5>H5 - 20px Proxima Nova Bold</h5>
											<h6>H6 - 18px Proxima Nova Bold</h6>
											
										</div><!-- .item -->
									</div><!-- .col -->
									<div class="col-2 col sm-col-1">
										<div class="item">
										
											<p>
												Paragraph - 16px Proxima Nova Regular - #1d1d1d - Sed pretium nisi ac massa blandit placerat. Sed vel sem varius, blandit tellus in, accumsan orci. 
												Sed accumsan aliquam lobortis. Mauris nulla sapien, rutrum et arcu a, convallis varius risus. Cras quis laoreet eros. Pellentesque varius mollis urna, 
												ut malesuada neque finibus eget. Mauris cursus finibus ante et ultrices. Sed fermentum.
											</p>
										
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col-2 col sm-col-1">
										<div class="item">
											
											<h4>Unordered List</h4>
											
											<ul>
												<li>Donec sollicitudin ipsum hendrerit nulla elementum vitae hendrer.</li>
												<li>Donec sollicitudin ipsum hendrerit nulla elementum vitae hendrer.</li>
												<li>Donec sollicitudin ipsum hendrerit nulla elementum vitae hendrer.</li>
											</ul>
											
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col-2 col sm-col-1">
										<div class="item">
											
											<h4>Ordered List</h4>
											
											<ol>
												<li>Donec sollicitudin ipsum hendrerit nulla elementum vitae hendrer.</li>
												<li>Donec sollicitudin ipsum hendrerit nulla elementum vitae hendrer.</li>
												<li>Donec sollicitudin ipsum hendrerit nulla elementum vitae hendrer.</li>
											</ol>										
											
										</div><!-- .item -->
									</div><!-- .col -->
									
								</div><!-- .grid -->
								
								<a href="#link" class="inline">Link Color</a>
								<br />
								<a href="#hover" class="inline hover">Hover Color</a>
								<br />
								<a href="#visited" class="inline visited">Visited Color</a>
								<br />
								
							</div><!-- .article-body -->
						</div><!-- .content -->
						
					</div><!-- .main-body -->
				</div><!-- .sw -->
			</section>
			
			
		</div><!-- .body -->
		
	</article>

<?php include('inc/i-footer.php'); ?>