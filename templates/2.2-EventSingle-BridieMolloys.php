<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero lazybg">
		<img src="../assets/bin/images/temp/inner-hero.jpg" alt="Image Alt">
	</div><!-- .hero -->

	<article>
		
		<div class="body">
		
			<div class="body-header d-bg red-bg">
				<div class="section-header">
					<h1 class="title">Event Title</h1>
					<span class="subtitle h5-style">Event Subtitle</span>
				</div><!-- .section-header -->
			</div><!-- .body-header -->
		
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#">Home</a>
					<a href="#">Event Subtitle</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<section>
				<div class="sw">
					<div class="main-body">
						
						<div class="content">
						
							<div class="article-head-meta">
								<time datetime="2014-12-14" class="meta t-fa fa-calendar">Sunday December 14, 2014</time>
								<span class="meta t-fa fa-clock-o">7:00PM - 10:00PM</span>
								<span class="meta t-fa fa-map-marker">Bridie Molloy's | <button class="view-map">MAP</button></span>
							</div><!-- .article-head-meta -->
							
						<!-- map -->
						<div class="gmap event-map">
							<div class="map" data-center="47.524755,-52.793752" data-zoom="15" data-markers='[{"title":"JAC","position":"47.524755,-52.793752"}]'></div>
						</div><!-- .gmap -->
							
							<div class="event-map-wrap">
								<!-- embedded google map, looks prettier than using javascript API -->
							</div><!-- .event-map-wrap -->
						
							<div class="article-body">
								<p>
									Sed pretium nisi ac massa blandit placerat. Sed vel sem varius, blandit tellus in, accumsan orci. Sed accumsan aliquam lobortis. 
									Mauris nulla sapien, rutrum et arcu a, convallis varius risus. Cras quis laoreet eros. Pellentesque varius mollis urna, ut malesuada 
									neque finibus eget. Mauris cursus finibus ante et ultrices. Sed fermentum venenatis sem, eu accumsan risus hendrerit vitae. 
									Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque congue purus sed 
									ligula faucibus fringilla. 
								</p>

								<p>
									Duis commodo at felis et fermentum. Donec sollicitudin ipsum hendrerit nulla elementum, vitae hendrerit diam finibus. Donec nec sollicitudin 
									augue, a viverra magna. Nulla cursus turpis ac velit feugiat, ut faucibus felis imperdiet. Nulla ante dui, ultrices ac commodo in, laoreet 
									cursus nisl. Cras sed ex diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse 
									cursus justo vitae nibh pretium commodo.
								</p>

								<p>
									Phasellus faucibus urna at ex semper venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non eros maximus, 
									consequat diam a, placerat nisl. Etiam vel dignissim nunc. Curabitur sagittis ipsum non euismod maximus. Sed aliquam cursus sem, a 
									fringilla risus posuere ut. Pellentesque molestie mi pellentesque, mattis arcu posuere, vestibulum libero. Nunc rutrum venenatis interdum. 
									Integer fermentum nisi sit amet molestie tempus. 
								</p>
							</div><!-- .article-body -->
						</div><!-- .content -->
						
						<aside class="sidebar">
							<?php include('inc/i-archives.php'); ?>
						</aside>
						
					</div><!-- .main-body -->
				</div><!-- .sw -->
			</section>
			
			
		</div><!-- .body -->
		
	</article>

<?php include('inc/i-footer.php'); ?>