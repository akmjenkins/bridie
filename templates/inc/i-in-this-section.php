<div class="in-this-section-mod">
	<div class="title d-bg red-bg">
		<h5>In This Section</h5>	
	</div><!-- .title -->
	
	<div class="expanded start-expanded">
		<div class="acc-item-content">	
			<ul>
				<li><a href="#">Page Link One</a></li>
				<li><a href="#">Page Link Two</a></li>
				<li class="selected"><a href="#">Page Link Three</a></li>
				<li><a href="#">Page Link Four</a></li>
				<li><a href="#">Page Link Five</a></li>
			</ul>
		</div><!-- .acc-item-content -->
	</div><!-- .expanded -->
	
</div><!-- .in-this-section -->
