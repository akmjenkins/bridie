		<div class="nav">
			<div class="sw">
				
				<nav>
					<ul>
						<li><a href="#">Our Irish Pubs</a></li>
						<li><a href="#">Live Music</a></li>
						<li><a href="#">Menu</a></li>
						<li><a href="#">Promotions</a></li>
						<li><a href="#">Gallery</a></li>
						<li><a href="#">The Latest</a></li>
					</ul>
				</nav>
			
				<div class="nav-top">
					<a href="#">Gift Cards</a>
					<a href="#">Services</a>
					<a href="#">Contact</a>
					<a href="#">Comments</a>
					
					<?php include('i-social.php'); ?>
					<button title="Search" class="toggle-search circle-button sm inverse search t-fa-abs fa-search">Search Bridie Molloy's</button>
					<form action="/" method="get" class="single-form search-form">
						<fieldset>
							<input type="text" name="s" placeholder="Search Bridie Molloy's...">
							<button class="t-fa-abs fa-search">Search</button>
						</fieldset>
					</form>
				</div><!-- .nav-top -->
			</div><!-- .sw -->
		</div><!-- .nav -->