<?php
	//you can remove these, I only included it so I could simulate WordPress conditionals while I was building the template
	function is_home() {
		global $bodyclass;
		return preg_match('/home/',$bodyclass);
	}

	function is_404() {
		global $bodyclass;
		return preg_match('/error404/',$bodyclass);
	}
?>
<!doctype html>
<html lang="en">

	<head>
		<title>Bridie Molloys</title>
		<meta charset="utf-8">
		
		<!-- modernizr - minus the shiv, still useful for adding tests -->
		<script src="../assets/bin/js/lib/modernizr/modernizr.js"></script>
		
		<!-- Roboto -->
		<link href="//fonts.googleapis.com/css?family=Roboto:500,300,500italic,400" rel="stylesheet">
		
		<!-- favicons -->
		<link rel="icon" type="image/x-icon"  href="../assets/bin/images/favicons/favicon.ico">
		<link rel="icon" type="image/png"  href="../assets/bin/images/favicons/favicon-32.png">
		<link rel="icon" href="../assets/bin/images/favicons/favicon-32.png" sizes="32x32">
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="../assets/bin/images/favicons/favicon-152.png">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/bin/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="../assets/bin/images/favicons/favicon-120.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/bin/images/favicons/favicon-114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/bin/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" href="../assets/bin/images/favicons/favicon-114.png">	
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="../assets/bin/images/favicons/favicon-144.png">
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0">
		
		<!-- pikaday CSS, JS is in main.min.js -->
		<link rel="stylesheet" href="../assets/bin/lib/pikaday/pikaday.css">
		
		<link rel="stylesheet" href="../assets/bin/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body class="<?php echo $bodyclass; ?>">

		<!-- nav -->
		<?php include('i-nav.php'); ?>
	
		<div class="page-wrapper">	
			<header>
				<div class="sw">
					
					<button class="toggle-nav">
						<span>&nbsp;</span> Menu
					</button>
					
					<a href="#" class="header-logo">
						<!-- data-ar = "100" because the logo is square -->
						<span class="ar" data-ar="100">Bridie Molloy's</span>
					</a>
				</div><!-- .sw -->
			</header>