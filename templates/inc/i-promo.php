<div class="promo lazybg d-bg" data-src="../assets/bin/images/temp/promo-1.jpg">
	<div class="section-header big center">
		<span class="title h1-style">Plan Your Night Out Today</span>
		<p>Purcahse a gift card today and experience a relaxing, fun-filled night out.</p>
		
		<a href="#" class="button big">Purchase Now</a>
	</div><!-- .section-header -->
</div><!-- .promo -->