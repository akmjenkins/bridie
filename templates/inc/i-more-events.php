				<a class="event-list-item" data-eventid="1" ref="#">
					<div class="sw">
						<article>
							<div class="img">
								<div class="ar lazybg" data-ar="100">
									<img src="../assets/bin/images/temp/article-1.jpg" alt="article-1">
								</div>
							</div><!-- .img -->
						
							<div class="article-body">
								<div class="article-content">
								
									<div class="article-head-meta">
										<time datetime="2014-12-14" class="meta t-fa fa-calendar">Sunday December 14, 2014</time>
										<span class="meta t-fa fa-clock-o">7:00PM - 10:00PM</span>
										<span class="meta t-fa fa-map-marker">Bridie Molloy's</span>
									</div><!-- .article-head-meta -->
									
									<h2>Praesent eu Laoreet Lectus</h2>
									
									<p>
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
										Integer eget ante sed risus gravida vestibulum id ac ante. Nullam erat dui, viverra quis nulla quis, feugiat viverra diam.
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
										Integer eget ante sed risus gravida vestibulum id ac ante. Nullam erat dui, viverra quis nulla quis, feugiat viverra diam.
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
									</p>	
									
								</div><!-- .article-content -->
								<span class="button grey">Read More</span>
							</div><!-- .article-body -->
							
						</article>
					</div><!-- .sw -->
				</a><!-- .event-list-item -->
				<a class="event-list-item" data-eventid="2" href="#">
					<div class="sw">
						<article>
							<div class="img">
								<div class="ar lazybg" data-ar="100">
									<img src="../assets/bin/images/temp/article-1.jpg" alt="article-1">
								</div>
							</div><!-- .img -->
						
							<div class="article-body">
								<div class="article-content">
									<div class="article-head-meta">
										<time datetime="2014-12-14" class="meta t-fa fa-calendar">Sunday December 14, 2014</time>
										<span class="meta t-fa fa-clock-o">7:00PM - 10:00PM</span>
										<span class="meta t-fa fa-map-marker">Bridie Molloy's</span>
									</div><!-- .article-head-meta -->
									
									<h2>Praesent eu Laoreet Lectus</h2>
									
									<p>
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
										Integer eget ante sed risus gravida vestibulum id ac ante.
									</p>								
								</div><!-- .article-content -->
								<span class="button grey">Read More</span>	
							</div><!-- .article-body -->
							
							
						</article>
					</div><!-- .sw -->
				</a><!-- .event-list-item -->
				<a class="event-list-item" data-eventid="3" href="#">
					<div class="sw">
						<article>
							<div class="img">
								<div class="ar lazybg" data-ar="100">
									<img src="../assets/bin/images/temp/article-1.jpg" alt="article-1">
								</div>
							</div><!-- .img -->
						
							<div class="article-body">
								<div class="article-content">
									<div class="article-head-meta">
										<time datetime="2014-12-14" class="meta t-fa fa-calendar">Sunday December 14, 2014</time>
										<span class="meta t-fa fa-clock-o">7:00PM - 10:00PM</span>
										<span class="meta t-fa fa-map-marker">Bridie Molloy's</span>
									</div><!-- .article-head-meta -->
									
									<h2>Praesent eu Laoreet Lectus eu Laoreet Praesent Lectus</h2>
									
									<p>
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
										Integer eget ante sed risus gravida vestibulum id ac ante. Nullam erat dui, viverra quis nulla quis, feugiat viverra diam.
										Maecenas posuere vitae ex at tristique. Aliquam semper laoreet eleifend. In hac habitasse platea dictumst. 
									</p>		
								</div><!-- .article-content -->
								<span class="button grey">Read More</span>
								
							</div><!-- .article-body -->
							
						</article>
					</div><!-- .sw -->
				</a><!-- .event-list-item -->
