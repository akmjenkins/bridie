<div class="archives-mod">
	<div class="title d-bg red-bg">
		<h5>Archives</h5>	
	</div><!-- .title -->
	
	<div class="acc with-indicators">
		<div class="acc-item">
			<div class="acc-item-handle">
				2014 (5)
			</div><!-- .acc-item-handle -->
			<div class="acc-item-content">
				<ul>
					<li class="selected"><a href="#">October (3)</a></li>
					<li><a href="#">September (2)</a></li>
				</ul>
			</div><!-- .acc-item-content -->
		</div><!-- .acc-item -->
		<div class="acc-item">
			<div class="acc-item-handle">
				2013 (10)
			</div><!-- .acc-item-handle -->
			<div class="acc-item-content">
				<ul>
					<li><a href="#">October (3)</a></li>
					<li><a href="#">September (2)</a></li>
					<li><a href="#">September (2)</a></li>
					<li><a href="#">September (2)</a></li>
				</ul>
			</div><!-- .acc-item-content -->
		</div><!-- .acc-item -->
		<div class="acc-item">
			<div class="acc-item-handle">
				2012 (15)
			</div><!-- .acc-item-handle -->
			<div class="acc-item-content">
				<ul>
					<li><a href="#">October (3)</a></li>
					<li><a href="#">October (3)</a></li>
					<li><a href="#">October (3)</a></li>
					<li><a href="#">October (3)</a></li>
					<li><a href="#">September (2)</a></li>
				</ul>
			</div><!-- .acc-item-content -->
		</div><!-- .acc-item -->
	</div><!-- .acc -->
	
</div><!-- .archives -->
