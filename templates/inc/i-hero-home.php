<div class="hero lazybg" data-src="../assets/bin/images/temp/hero.jpg">
	<div class="sw">
		<div class="caption">
			
			<span class="h1-style">One Moment Here is Worth Many More</span>
			
			<p>
				A warm and welcoming Restaurant nestled beneath our traditionally 
				spirited Guinness Pub, ensures that a moment with us is  one to savour.
			</p>
			
			<a href="#" class="button big">Find Out More</a>
			
		</div><!-- .caption -->
	</div><!-- .sw -->
</div><!-- .hero -->