//load all required scripts
requirejs(
	[
		//include jQuery from here - no extra HTTP request
		'jquery',
		
		//pikaday
		'../../bower_components/pikaday/pikaday',
		
		'scripts/tim.microtemplate',
		'scripts/gmap',
		'scripts/anchors.external.popup',
		'scripts/device.pixel.ratio',
		'scripts/magnific.popup',
		'scripts/standard.accordion',
		'scripts/responsive.video',
		'scripts/custom.select',
		'scripts/aspect.ratio',
		'scripts/lazy.images',

		'scripts/search',
		'scripts/nav',
	],
	function($,Pikaday,microTemplate) {
	
		//load more events
		$('div.load-more-events').each(function() {
			var 
				el = $(this),
				button = $('.button',el),
				
				LOADING_CLASS = 'loading',
				methods = {
				
					getLastEventId: function() {
						return $('a.event-list-item').last().data('eventid');
					},
				
					loadEvents: function() {
					
						if(this.isLoading()) { return; }
					
						var self = this;
						this.showLoading(true);
						
						setTimeout(function() {
							$.ajax({
								url: el.data('src'),
								data: { id: self.getLastEventId() }
							})
								.done(function(r,status,jqXHR) {
									$('div.event-list').append(r);
									$(document).trigger('updateTemplate');
								})
								.always(function() { self.showLoading(false); });
						},2000);
					},
					
					isLoading: function() {
						return button.hasClass(LOADING_CLASS);
					},
				
					showLoading: function(show) {
						if(show) {
							button.addClass(LOADING_CLASS);
						} else {
							button.removeClass(LOADING_CLASS);
						}
					}
				};
			
			button.on('click',function() { methods.loadEvents(); });
		});
	
		//pikaday events calendar
		(function() {
		
			var 
				eventsCalendar = $('div.events-calendar'),
				currentEventElement = $('div.selected-event'),
				tmpl = $('div.events-calendar-tmpl',eventsCalendar).html(),
				url = eventsCalendar.data('src'),
				DISPLAY_FIRST_EVENT = true,
				currentEvent,
				cachedEventIds = {},
				cachedEvents = [],
				req,
				LOADING_CLASS = 'loading',
				picker,
				months = ['January','Feburary','March','April','May','June','July','August','September','October','November','December'],
				methods = {
				
					getEventIds: function(year,month) {
						var self = this;
						
						if(cachedEventIds[year] && cachedEventIds[year][month]) {
							this.updateCalendar(cachedEventIds[year][month]);
							return;
						}
						
						//always display the first event that comes back immediately
						$.ajax({
							url: url,
							data: {
								month:month,
								year:year
							}
						})
							.done(function(r,status,jqXHR) {
								if(!cachedEventIds[year]) {
									cachedEventIds[year] = {};
								}
								cachedEventIds[year][month] = r;
								self.updateCalendar(cachedEventIds[year][month]);
							});
					},
					
					getTds: function() {
						return $('td',eventsCalendar);
					},
					
					updateCalendar: function(eventIds) {
						
						this.getTds().each(function() {
							var 
								el  = $(this),
								day = el.data('day');
								
								//on all elements
								el.addClass('is-disabled');
								
							$.each(eventIds,function(i,eventIdObject) {
								if(+(eventIdObject.day) === +(day)) {
									el
										.addClass('has-event')
										.data('eventid',eventIdObject.id);
								}
							});
						});
						
						//always display the first event in the calendar immediately?
						if(DISPLAY_FIRST_EVENT) {
							this.loadEvent(eventIds.length && eventIds[0].id);
						} else {
							DISPLAY_FIRST_EVENT = true;
							this.updateSelectedCalendarDate();
						}
					},
					
					loadEvent: function(id) {
						var 
							eventObject,
							self = this,
							failed = function() {
								currentEventElement.html('<span class="no-event">Unable to retrieve event details.</span>');
							};
						
						$.each(cachedEvents,function(i,thisEventObject) {
							if(thisEventObject.id == id) {
								eventObject = thisEventObject;
								return false;
							}
						});
						
						req && req.abort();
						
						if(eventObject) {
							this.displayEvent(eventObject);
							return;
						}
						
						currentEventElement.addClass(LOADING_CLASS);
						
						setTimeout(function() {
							req = $.ajax({
								url: url,
								data: {id:id}
							})
								.done(function(r,status,jqXHR) {
									if(r && r.id && status === 'success') {
										var eventObject = r;
										
										if(!r.thumbnail) { r.thumbnail = ''; }
										eventObject.has_image = !!r.thumbnail ? 'has-thumbnail' : '';
										
										//cache the events in memory (up to 20)
										cachedEvents.unshift(eventObject);
										cachedEvents > 20 && cachedEvents.splice(20);
										
										self.displayEvent(eventObject);
										return;
									}
									
									if(status !== 'aborted') { failed(); }
								})
								.fail(function() { failed(); })
								.always(function() {
									currentEventElement.removeClass(LOADING_CLASS);
								});
						},0);
					},
					
					displayEvent: function(eventObject) {
						if(eventObject) {
							currentEvent = eventObject;
							currentEventElement.html(microTemplate(tmpl,eventObject));
							$(document).trigger('updateTemplate');
							
							if(picker._m != (+eventObject.month)-1 || picker._y != eventObject.year) {
								DISPLAY_FIRST_EVENT = false;
								var date = new Date();
								date.setYear(eventObject.year);
								date.setMonth((+eventObject.month)-1);
								picker.gotoDate(date);
							}
							
						} else {
							currentEvent = null;
							currentEventElement.html('<span class="no-event">No Events For '+months[picker._m]+' ' + picker._y + '</span>');
						}
						
						this.updateSelectedCalendarDate();
					},
					
					updateSelectedCalendarDate: function() {
						this
							.getTds()
								.removeClass('is-selected')
								.filter(function() {
									return currentEvent && $(this).data('eventid') == currentEvent.id;
								})
								.addClass('is-selected');
					}
					
				};
				
				eventsCalendar.each(function() {
				
					$('div.dp-flat',this).each(function() {
						var 
							startMonth = eventsCalendar.data('startmonth'),
							startYear = eventsCalendar.data('startyear'),
							defaultDate = new Date();
							
							startMonth && defaultDate.setMonth(startMonth-1);
							startYear && defaultDate.setYear(startYear);
						
						picker = new Pikaday({
							onDraw: function() { 
								methods.getEventIds(this._y,this._m+1);  
							},
							defaultDate: defaultDate
						});
						$(this).append(picker.el)
					});
					
					$(this)
						.on('click','td.has-event',function(e) {
							methods.loadEvent($(this).data('eventid'));
						})
						.on('click','.controls span',function() {
							methods.loadEvent($(this).data('eventid'));
						});
				});
		
		}());

		//event map
		$('button.view-map')
			.on('click',function(e) {
				$('div.event-map').each(function() {
					var
						el = $(this),
						map = el.children('div.map').data('map')
						
					el.toggleClass('visible');
					google.maps.event.trigger(map.map,'resize');
					map.map.setCenter(map.mapOptions.center);
				});
			});		
	}
);