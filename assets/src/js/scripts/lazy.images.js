define([
	'scripts/debounce'
],function(debounce) {

	var
		$window = $(window),
		$document = $(document),
		d = debounce(),
		windowHeight = $window.innerHeight(),
		updateHeight = false,
		methods = {
		
			getImages: function() {
				return $('.lazybg').filter(function() { return $(this).parent().is(':visible') });
			},
		
			updateLazyImages: function() {
				
				if(updateHeight) {
					windowHeight = $window.innerHeight();
				}
				var topOfLazyImage = $window.scrollTop()+windowHeight;

				this.loadImages(methods.getImages().filter(function() {
						var el = $(this);
						return !el.hasClass('loading') && (el.offset().top-topOfLazyImage < 0 || el.hasClass('immediate'));
					})
				);
				
			},
		
			loadImages: function(images) {
			
				$.each(images,function() {
					var 
						childImg,
						img = $(this),
						source = img.data('src');
						
					if(!source) {
						img.find('img').each(function() {
							source = this.src;
							return false;
						});
					}
						
					if(source) {
						img.addClass('loading');
						
						$('<img/>')
							.on('load error',function() {
								img
									.css({backgroundImage:'url('+source+')'})
									.addClass('loaded');
							})
							.attr('src',source)
					}
						
				});
				
			}		
		};
		
	//can be triggered when needed (e.g. in the case of AJAX updates to the dom)
	//via $(document).trigger('updateTemplate.lazyimages'); or just $(document).trigger('updateTemplate');
	$window.on('scroll load resize updateTemplate.lazyimages',function(e) { 
		upateHeight = e.type === 'resize';
		d.requestProcess(function() {
			methods.updateLazyImages();
		}); 
	}).trigger('updateTemplate.lazyimages');

	//no public API
	return {};
});