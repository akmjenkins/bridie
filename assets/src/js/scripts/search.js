define([
	'jquery',
],function($) {
		
	var 
		$html = $('html'),
		$form = $('div.search-form'),
		$input = $form.children('input'),
		showclass = 'show-search',
		methods = {
		
			showForm: function() {
				$html.addClass(showclass);
				$input.val('');
				Modernizr.touch || $input.focus();

			},
			
			hideForm: function() {
				var self = this;
				$html.removeClass(showclass);
				$input.blur();
			},
			
			isVisible: function() {
				return $html.hasClass(showclass);
			},
			
			toggleForm: function() {
				if(!this.isVisible()) {
					return this.showForm();
				}
				
				return this.hideForm();
			}
	
		};
	
	$(document)
		.on('click','.toggle-search',function(e) {
			e.preventDefault();
			methods.toggleForm();
		})
		.on('keydown',function(e) {
			if(e.result !== false && e.keyCode ===  27 && methods.isVisible()) {
				methods.hideForm();
				return false;
			}
		});

	return methods;

});