define([
],function() {

	var
		$window = $(window),
		methods = {
		
			getDivs: function() {
				return $('.ar').filter(function() { return !$(this).hasClass('ar-ready'); });
			},
		
			updateDivs: function() {
				this.getDivs().each(function() {
					var 
						el = $(this),
						ar = el.data('ar');
						
					el
						.addClass('ar-ready')
						.css({paddingBottom:ar+'%'});
				});
				
			}
			
		};
		
	//can be triggered when needed (e.g. in the case of AJAX updates to the dom)
	//via $(document).trigger('updateTemplate.aspect'); or just $(document).trigger('updateTemplate');
	$window.on('load resize updateTemplate.aspectRatio',function(e) { 
		methods.updateDivs();
	})
		//perform template update immediately
		.trigger('resize');

	//no public API
	return {};
});